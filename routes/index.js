var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'WordApplication' });
});

router.get('/regist', function(req, res, next) {
  res.render('regist');
});
router.get('/test', function(req, res, next) {
  res.render('test');
});
router.get('/manage', function(req, res, next) {
  res.render('manage');
});

module.exports = router;
