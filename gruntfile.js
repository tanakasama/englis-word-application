module.exports = function(grunt){
    grunt.initConfig({
        express: {
            dev: {
              options: {
                script: 'bin/www'
              }
            }
        },
        sass: {
            dist: {
                files: {
                    "public/css/common.css": "develop/scss/common.scss",
                    "public/css/top.css": "develop/scss/top.scss",
                    "public/css/regist.css": "develop/scss/regist.scss",
                    "public/css/test.css": "develop/scss/test.scss",
                    "public/css/manage.css": "develop/scss/manage.scss"
                }
            }
        },
        browserify: {
            dist: {
                files: {
                    "public/js/common.js": "develop/js/common.js",
                    "public/js/manage.js": "develop/js/manage.js"
                }
            }
        },
        watch: {
            scripts: {
                files: [
                    'develop/scss/**',
                    'develop/js/**'
                ],
                tasks: ['sass', 'browserify', 'cssmin']
            }
        },
        cssmin: {
          target: {
            files: {
              'public/css/common.css': 'public/css/common.css',
              'public/css/top.css': 'public/css/top.css',
              'public/css/regist.css': 'public/css/regist.css',
              'public/css/test.css': 'public/css/test.css',
              'public/css/manage.css': 'public/css/manage.css'
            }
          }
        }
    });

    // 使用モジュールの読み込み
    grunt.loadNpmTasks('grunt-express-server');
    grunt.loadNpmTasks('grunt-shell');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-browserify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // task登録
    grunt.registerTask('default', [
        'express',
        'watch'
    ]);
};