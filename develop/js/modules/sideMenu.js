// 左サイドのハンバーガーメニューを制御するモジュール
var $ = require('jquery');

module.exports = {
	duration: 100,
	// 初期化用の関数
	init: function($content, $trigger) {
		var self = this;

		$trigger.data({
			'content': $content,
			'left': -$content.width(),
			'duration': self.duration
		});
	},
	// 開閉用の関数
	toggle: function() {
		var $this = $(this);
		var $content = $this.data('content');
		var duration = $this.data('duration');

		if ($content.css('left')　=== '0px') {
			$content.animate({
				left: $this.data('left')
			}, duration);
		} else {
			$content.animate({
				left: 0
			}, duration);
		}
	}
};