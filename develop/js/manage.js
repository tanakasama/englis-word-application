var $ = require('jquery');
var _ = require('underscore');

// 一行分のアイテム数
var ROW_ITEM_LENGTH = 3;

var $doc = $(document);
var $wordList = $('#js-wordList');
var $wordListItem = $('.js-wordListItem');
var $wordListItemTable = $('.js-wordListItemTable');


var setWordListHeight = function($table) {
	var rowLength = Math.ceil($table.length / ROW_ITEM_LENGTH);
	var i;
	var rowElemList = [];

	for (i = 0; i < rowLength; i++) {
		var firstNum = i * ROW_ITEM_LENGTH;
		var lastNum = firstNum + ROW_ITEM_LENGTH;

		rowElemList.push($table.slice(firstNum, lastNum));
	}

	_.forEach(rowElemList, function(list, i) {
		var maxHeight = _.max(list, function(item) {
			return $(item).height();
		});

		$(list).parent('.js-wordListItem').height($(maxHeight).outerHeight());
	});
};

$wordList
	.on('click', '.js-removeButton', function () {
		var $target = $(this).parent('.js-wordListItem');

		$target.animate({
			width: '0px',
			opacity: 0
		}, 200, function() {
			$target.remove();
			setWordListHeight($('.js-wordListItemTable'));
		});
	});

$wordList
	.on('keydown', '.js-editTextArea', function (e) {
		// エンターキー押下時、フォーカスを外す
		if (e.keyCode === 13) {
			$(this)[0].blur();

			return false;
		}
	});

setWordListHeight($wordListItemTable);