var $ = require('jquery');

/* 自作モジュールの読み込み */
var sideMenu = require('./modules/sideMenu.js');

/* DOMの取得 */
// 左サイドのハンバーガーメニュー
var $sideMenu = $('#js-sideMenu');
var $sideMenuTrigger = $sideMenu.find('.js-sideMenuTrigger');

/* コントローラ */
// 左サイドのハンバーガーメニュー
sideMenu.init($sideMenu, $sideMenuTrigger);
$sideMenuTrigger.on('click', sideMenu.toggle);